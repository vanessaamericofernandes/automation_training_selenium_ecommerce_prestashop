package steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.CartReviewPage;
import pages.CheckoutPage;
import pages.HomePage;
import pages.LoginPage;
import pages.OrderPage;
import pages.ProductPage;
import pages.ProductPageModal;
import util.Functions;

public class OrderProduct_Clean {

	private static RemoteWebDriver driver;
	private HomePage homePage = new HomePage(driver);

	@Before
	public static void init() throws Exception {

		// System.setProperty("webdriver.chrome.driver",
		// "C:\\WebDrivers\\ChromeDriver\\v83\\chromedriver.exe");
		// driver = new ChromeDriver();
		ChromeOptions options = new ChromeOptions();
		driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), options);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@Given("I am in the Home Page")
	public void i_am_in_the_home_page() {
		homePage.loadHomePage();
		assertThat(homePage.getPageTitle(), is("Loja de Teste"));
	}

	@When("I am not logged in")
	public void i_am_not_logged_in() {
		if (homePage.isTheUserLoggedIn()) {
			loginPage.clickOnSignOutButton();
		}
		assertThat(homePage.isTheUserLoggedIn(), is(false));
	}

	@Then("I see a list of {int} available products")
	public void i_see_a_list_of_available_products(Integer int1) {
		assertThat(homePage.countProducts(), is(int1));
	}

	@Then("the cart is empty")
	public void the_cart_is_empty() {
		assertThat(homePage.getNumberOfProductsInTheCart(), is(0));
	}

	LoginPage loginPage;

	@When("I am logged-in with the following credentials {string}, {string}, {string}")
	public void i_am_logged_in_with_the_following_credentials(String userEmail, String userPassword,
			String userFullName) {
		loginPage = homePage.clickOnSignInButton();
		loginPage.fillUserEmail(userEmail);
		loginPage.fillUserPassword(userPassword);
		loginPage.clickOnSignInButton();

		assertThat(homePage.isLoggedInUserFullNameDisplayed(userFullName), is(true));

		homePage.loadHomePage();
	}

	ProductPage productPage;

	@When("I select a product in a given {int}")
	public void i_select_a_product_in_a_given_position(Integer index) {
		productPage = homePage.selectProduct(index);
	}

	ProductPageModal productPageModal;

	@When("add it into the cart by choosing the size {string}, color {string} and quantity {int}")
	public void add_it_into_the_cart_by_choosing_the_size_color_and_quantity(String productSize, String productColor,
			Integer productQuantity) {
		// Select size
		productPage.selectSizeFromDropdown(productSize);

		// Select color
		if (!productColor.equals("N/A"))
			productPage.selectColor(productColor);

		// Select quantity
		productPage.selectQuantity(productQuantity);

		// Add into the car
		productPageModal = productPage.clickOnAddToCartButton();

		assertTrue(
				productPageModal.getProductAddedMessage().endsWith("Product successfully added to your shopping cart"));
	}

	@Then("new_the selected product is displayed on review shopping cart screen with the following values: name {string}, price {string}, size {string}, color {string}, quantity {int}, subtotal based on price and quantity")
	public void new_the_selected_product_is_displayed_on_review_shopping_cart_screen_with_the_following_values_name_price_size_color_quantity_subtotal_based_on_price_quantity(
			String productName, String productPrice, String productSize, String productColor, Integer productQuantity) {

		assertThat(productPageModal.getProductDescription().toUpperCase(), is(productName.toUpperCase()));

		Double foundProductPriceDouble = Double.parseDouble(productPageModal.getProductPrice().replace("$", ""));
		Double expectedProductPriceDouble = Double.parseDouble(productPrice.replace("$", ""));
		assertThat(foundProductPriceDouble, is(expectedProductPriceDouble));

		assertThat(productPageModal.getSize(), is(productSize));

		if (!productColor.equals("N/A"))
			assertThat(productPageModal.getColor(), is(productColor));

		assertThat(productPageModal.getQuantity(), is(productQuantity));

		String foundSubtotalString = productPageModal.getSubTotal();
		foundSubtotalString = foundSubtotalString.replace("$", "");
		Double foundSubtotalDouble = Double.parseDouble(foundSubtotalString);

		Double expectedSubtotalDouble = productQuantity * expectedProductPriceDouble;

		assertThat(foundSubtotalDouble, is(expectedSubtotalDouble));
	}

	CartReviewPage cartReviewPage;

	@When("proceed to checkout")
	public void proceed_to_checkout() {
		cartReviewPage = productPageModal.clickOnProceedToCheckoutButton();
	}

	@Then("I see all items I have added into the shopping cart as well as their following details: {string}, {string}, {string}, {string}, {int}, subtotal, {double} and total with and without {double} included")
	public void i_see_all_items_i_have_added_into_the_shopping_cart_as_well_as_their_following_details_subtotal_shipping_value_and_total_with_and_without_taxes_included(
			String productName, String productPrice, String productSize, String productColor, Integer productQuantity,
			Double shippingValue, Double taxes) {

		assertThat(cartReviewPage.getProductName().toUpperCase(), is(productName.toUpperCase()));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getProductPrice()),
				is(Functions.removeDollarSignAndReturnDouble(productPrice)));
		assertThat(cartReviewPage.getProductSize(), is(productSize));

		if (!productColor.equals("N/A"))
			assertThat(cartReviewPage.getProductColor(), is(productColor));

		assertThat(Integer.parseInt(cartReviewPage.getProductQuantity()), is(productQuantity));

		Double productSubtotal = Functions.removeDollarSignAndReturnDouble(productPrice) * productQuantity;

		int expected_totalNumberOfItems = productQuantity;
		Double expected_subTotal = productSubtotal;

		Double expected_totalTaxExclTotal = expected_subTotal + shippingValue;

		Double expected_totalTaxInclTotal = expected_totalTaxExclTotal + taxes;

		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getProductSubtotal()), is(productSubtotal));
		assertThat(Functions.removeTextItemsAndReturnInt(cartReviewPage.getTotalNumberOfItems()),
				is(expected_totalNumberOfItems));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getSubtotalTotal()), is(expected_subTotal));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getShippingValue()), is(shippingValue));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getTotalTaxExclTotal()),
				is(expected_totalTaxExclTotal));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getTaxesTotal()), is(taxes));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getTotalTaxInclTotal()),
				is(expected_totalTaxInclTotal));
	}

	@When("I have added a product into the shopping cart: position {int}, size {string}, color {string} and quantity {int}")
	public void i_have_added_a_product_into_the_shopping_cart_position_size_color_and_quantity(Integer productPosition,
			String productSize, String productColor, Integer productQuantity) {
		i_select_a_product_in_a_given_position(productPosition);
		add_it_into_the_cart_by_choosing_the_size_color_and_quantity(productSize, productColor, productQuantity);
		proceed_to_checkout();
	}

	CheckoutPage checkoutPage;

	@When("I click on Proceed to Checkout button on Cart Review Page")
	public void i_click_on_proceed_to_checkout_button_on_cart_review_page() {
		checkoutPage = cartReviewPage.clickOnProceedToCheckoutButton();
	}

	@Then("I see Total including taxes details based on subtotal {string} * {int}, {double} and {double}")
	public void i_see_total_including_taxes_details_on_subtotal(String productPrice, Integer productQuantity,
			Double shippingValue, Double taxes) {
		Double productSubtotal = Functions.removeDollarSignAndReturnDouble(productPrice) * productQuantity;

		Double expected_totalTaxExclTotal = productSubtotal + shippingValue;

		Double expected_totalTaxInclTotal = expected_totalTaxExclTotal + taxes;

		assertThat(Functions.removeDollarSignAndReturnDouble(checkoutPage.getTotalTaxInclTotal()),
				is(expected_totalTaxInclTotal));
	}

	@Then("I see the logged-in {string} on the registered address on Address Step")
	public void i_see_the_logged_in_user_full_name_on_the_registered_address_on_address_step(String userFullName) {
		assertTrue(checkoutPage.getClientName().startsWith(userFullName));
	}

	@Then("I am able to go to Shipping Method step by clicking on Continue button to check the {double}")
	public void i_am_able_to_go_to_shipping_method_step_by_clicking_on_continue_button_to_check_the_shipping_value(
			Double shippingValue) {
		checkoutPage.clickOnAddressContinueButton();

		String found_ShippingValue = checkoutPage.getShippingValue();
		found_ShippingValue = Functions.removeText(found_ShippingValue, " tax excl.");
		Double found_ShippingValue_Double = Functions.removeDollarSignAndReturnDouble(found_ShippingValue);

		assertThat(found_ShippingValue_Double, is(shippingValue));
	}

	@Then("I am able to see and select My Carrier shipping method")
	public void i_am_able_to_see_and_select_my_carrier_shipping_method() {
		// TODO My Carrier is the only available shipping method and it is selected by
		// default. Implement this method to either confirm My Carrier is selected or
		// choose another option when available.
	}

	@Then("I am able to go to Payment step by clicking on Continue button")
	public void i_am_able_to_go_to_payment_step_by_clicking_on_continue_button() {
		checkoutPage.clickOnContinueToPaymentButton();
	}

	@Then("I am able to select Pay by Check option and verify pay by check amount based on subtotal {string} * {int}, {double} and {double}")
	public void i_am_able_to_select_pay_by_check_option_veerify_pay_by_check_amount_based_on_subtotal(
			String productPrice, Integer productQuantity, Double shippingValue, Double taxes) {
		checkoutPage.selectPayByCheckRadioButton();

		String payByCheckAmount = Functions.removeText(checkoutPage.getPayByCheckAmount(), " (tax incl.)");
		Double payByCheckAmountDouble = Functions.removeDollarSignAndReturnDouble(payByCheckAmount);

		Double productSubtotal = Functions.removeDollarSignAndReturnDouble(productPrice) * productQuantity;

		Double expected_totalTaxExclTotal = productSubtotal + shippingValue;

		Double expected_totalTaxInclTotal = expected_totalTaxExclTotal + taxes;

		assertThat(payByCheckAmountDouble, is(expected_totalTaxInclTotal));
	}

	@Then("I am able to select I agree Terms and Conditions option")
	public void i_am_able_to_select_i_agree_terms_and_conditions_option() {
		checkoutPage.selectIAgreeTermsAndConditionsCheckbox();
		assertTrue(checkoutPage.isIAgreeTermsAndConditionsCheckboxSelected());
	}

	OrderPage orderPage;

	@Then("I am able to complete the product ordering by clicking on Order with and obligation to Pay button")
	public void i_am_able_to_complete_the_product_ordering_by_clicking_on_order_with_and_obligation_to_pay_button() {
		orderPage = checkoutPage.clickOnOrderWithAnObligationToPayButton();
		assertTrue(orderPage.getOrderConfirmationMessage().endsWith("Your order is confirmed".toUpperCase()));
	}

	@When("I ordered a product that was in the shopping cart: position {int}, size {string}, color {string} and quantity {int}")
	public void i_ordered_a_product_that_was_in_the_shopping_cart_position_size_color_and_quantity(
			Integer productPosition, String productSize, String productColor, Integer productQuantity) {
		i_have_added_a_product_into_the_shopping_cart_position_size_color_and_quantity(productPosition, productSize,
				productColor, productQuantity);
		i_click_on_proceed_to_checkout_button_on_cart_review_page();
		checkoutPage.clickOnAddressContinueButton();
		checkoutPage.clickOnContinueToPaymentButton();
		checkoutPage.selectPayByCheckRadioButton();
		i_am_able_to_select_i_agree_terms_and_conditions_option();
		i_am_able_to_complete_the_product_ordering_by_clicking_on_order_with_and_obligation_to_pay_button();
	}

	@Then("I see the logged-in {string}")
	public void i_see_the_logged_in_user_email(String userEmail) {
		assertThat(orderPage.getEmail(), is(userEmail));
	}

	@Then("the subtotal is based on {string} * {int}")
	public void the_subtotal_is_based_on_product_price_product_quantity(String productPrice, Integer productQuantity) {
		Double expected_subTotal = Functions.removeDollarSignAndReturnDouble(productPrice) * productQuantity;
		assertThat(Functions.removeDollarSignAndReturnDouble(orderPage.getSubtotal()), is(expected_subTotal));
	}

	@Then("the Total including taxes is based on subtotal {string} * {int}, {double} and {double}")
	public void the_total_including_taxes_is_based_on_subtotal_shipping_value_taxes_values(String productPrice,
			Integer productQuantity, Double shippingValue, Double taxes) {
		Double productSubtotal = Functions.removeDollarSignAndReturnDouble(productPrice) * productQuantity;

		Double expected_totalTaxExclTotal = productSubtotal + shippingValue;

		Double expected_totalTaxInclTotal = expected_totalTaxExclTotal + taxes;

		assertThat(Functions.removeDollarSignAndReturnDouble(orderPage.getTotalTaxInclTotal()),
				is(expected_totalTaxInclTotal));
	}

	@Then("the {string} is the one I selected on Checkout > Payment step")
	public void the_payment_method_is_the_one_i_selected_on_checkout_payment_step(String paymentMethod) {
		assertThat(orderPage.getPaymentMethod(), is(paymentMethod));
	}

	@After(order = 1)
	public void screenCapture(Scenario scenario) {
		File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String scenarioId = scenario.getId().substring(scenario.getId().lastIndexOf(".feature:") + 9);
		String fileName = "resources/screenshots/" + scenario.getName() + "_" + scenarioId + "_" + scenario.getStatus()
				+ ".png";

		try {
			FileUtils.copyFile(screenshotFile, new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@After(order = 0)
	public static void close() {
		driver.quit();
	}
}
