package homepage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import base.BaseTests;
import pages.CartReviewPage;
import pages.CheckoutPage;
import pages.LoginPage;
import pages.OrderPage;
import pages.ProductPage;
import pages.ProductPageModal;
import util.Functions;

public class HomePageTests extends BaseTests {

	@Test
	public void testCountProducts() {
		loadInitialPage();
		assertThat(homePage.countProducts(), is(8));
	}

	@Test
	public void testValidateCartIsEmpty() {
		int productsInTheCart = homePage.getNumberOfProductsInTheCart();
		assertThat(productsInTheCart, is(0));
	}

	ProductPage productPage;
	String productNameOnProductPage;
	@Test
	public void testIfProductDescriptionAndProductValueOnHomePageMatchTheProductDescriptionAndProductValueOnProductPage() {
		int index = 0;
		String productNameOnHomePage = homePage.getProductName(index);
		String productPriceOnHomePage = homePage.getProductPrice(index);

		productPage = homePage.selectProduct(index);

		productNameOnProductPage = productPage.getProductName();
		String productPriceOnProductPage = productPage.getProductPrice();

		assertThat(productNameOnHomePage.toUpperCase(), is(productNameOnProductPage.toUpperCase()));
		assertThat(productPriceOnHomePage, is(productPriceOnProductPage));
	}

	LoginPage loginPage;
	String userEmail = "teste@teste.com";
	@Test
	public void testLoginIsSuccessful() {

		loginPage = homePage.clickOnSignInButton();	

		loginPage.fillUserEmail(userEmail);

		loginPage.fillUserPassword("teste");
		
		loginPage.clickOnSignInButton();

		assertThat(homePage.isLoggedInUserFullNameDisplayed("Teste Testador"), is(true));
		
		loadInitialPage();
	}
	
	//The below method is to practice Mass Testing approach using cvs file and screen capture;
	@ParameterizedTest
	@CsvFileSource(resources = "/massTesting_login.csv", numLinesToSkip = 1, delimiter = ',')
	public void testLoginWithValidAndInvalidCredentials(String testName, String email, String password, String userName, String expectedResult) {
		
		loginPage = homePage.clickOnSignInButton();	

		loginPage.fillUserEmail(email);

		loginPage.fillUserPassword(password);
		
		loginPage.clickOnSignInButton();

		boolean successfulLogin;
		if(expectedResult.equals("pass"))
			successfulLogin = true;
		else
			successfulLogin = false;
		
		assertThat(homePage.isLoggedInUserFullNameDisplayed(userName), is(successfulLogin));
		
		screenCapture(testName, expectedResult);
		
		if(successfulLogin)
			loginPage.clickOnSignOutButton();
		
		loadInitialPage();
	}
	
	ProductPageModal productPageModal;
	@Test
	public void testAddProductToCart() {

		String productSize = "M";
		String productColor = "Black";
		int productQuantity = 2;

		if(!homePage.isLoggedInUserFullNameDisplayed("Teste Testador")) {
			testLoginIsSuccessful();
		}
		testIfProductDescriptionAndProductValueOnHomePageMatchTheProductDescriptionAndProductValueOnProductPage();

		//Select size
		productPage.selectSizeFromDropdown(productSize);

		//Select color
		productPage.selectColor(productColor);

		//Select quantity
		productPage.selectQuantity(productQuantity);

		//Add into the car
		productPageModal = productPage.clickOnAddToCartButton();

		//Validations
		assertTrue(productPageModal.getProductAddedMessage().endsWith("Product successfully added to your shopping cart"));
		assertThat(productPageModal.getProductDescription().toUpperCase(), is(productNameOnProductPage.toUpperCase()));
		assertThat(productPageModal.getSize(), is(productSize));
		assertThat(productPageModal.getColor(), is(productColor));
		assertThat(productPageModal.getQuantity(), is(productQuantity));	

		String productPriceString = productPageModal.getProductPrice().replace("$", "");
		Double productPrice = Double.parseDouble(productPriceString);

		String subTotalString = productPageModal.getSubTotal().replace("$", "");
		Double subTotal = Double.parseDouble(subTotalString);

		Double calculatedSubTotal = productQuantity * productPrice;
		assertThat(calculatedSubTotal, is(subTotal));
	}

	//Expected Values

	String expected_ProductName = "Hummingbird printed t-shirt";
	Double expected_ProductPrice = 19.12;
	String expected_ProductSize = "M";
	String expected_ProductColor = "Black";
	int expected_ProductQuantity = 2;
	Double expected_ProductSubtotal = expected_ProductPrice * expected_ProductQuantity;

	int expected_totalNumberOfItems = expected_ProductQuantity;
	Double expected_subTotal = expected_ProductSubtotal;
	Double expected_ShippingValue = 7.00;
	Double expected_totalTaxExclTotal = expected_subTotal + expected_ShippingValue;
	Double expected_totalTaxes = 0.00;
	Double expected_totalTaxInclTotal = expected_totalTaxExclTotal + expected_totalTaxes;
	String expected_clientName = "Teste Testador";

	CartReviewPage cartReviewPage;
	@Test
	public void testReviewItemsInTheCart() {
		testAddProductToCart();
		cartReviewPage = productPageModal.clickOnProceedToCheckoutButton();

		//Asserts Hamcrest
		assertThat(cartReviewPage.getProductName(), is(expected_ProductName));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getProductPrice()), is(expected_ProductPrice));
		assertThat(cartReviewPage.getProductSize(), is(expected_ProductSize));
		assertThat(cartReviewPage.getProductColor(), is(expected_ProductColor));
		assertThat(Integer.parseInt(cartReviewPage.getProductQuantity()), is(expected_ProductQuantity));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getProductSubtotal()),is(expected_ProductSubtotal));
		assertThat(Functions.removeTextItemsAndReturnInt(cartReviewPage.getTotalNumberOfItems()), is(expected_totalNumberOfItems));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getSubtotalTotal()), is(expected_subTotal));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getShippingValue()), is(expected_ShippingValue));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getTotalTaxExclTotal()), is(expected_totalTaxExclTotal));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getTaxesTotal()), is(expected_totalTaxes));
		assertThat(Functions.removeDollarSignAndReturnDouble(cartReviewPage.getTotalTaxInclTotal()), is(expected_totalTaxInclTotal));		
	}

	CheckoutPage checkoutPage;
	@Test
	public void testProceedToCheckout() {
		testReviewItemsInTheCart();
		checkoutPage = cartReviewPage.clickOnProceedToCheckoutButton();
		assertThat(Functions.removeDollarSignAndReturnDouble(checkoutPage.getTotalTaxInclTotal()), is(expected_totalTaxInclTotal));
		assertTrue(checkoutPage.getClientName().startsWith(expected_clientName));
		checkoutPage.clickOnAddressContinueButton();

		String found_ShippingValue = checkoutPage.getShippingValue();
		found_ShippingValue = Functions.removeText(found_ShippingValue, " tax excl.");
		Double found_ShippingValue_Double = Functions.removeDollarSignAndReturnDouble(found_ShippingValue);
		assertThat(found_ShippingValue_Double, is(expected_ShippingValue));

		checkoutPage.clickOnContinueToPaymentButton();
		checkoutPage.selectPayByCheckRadioButton();
		String payByCheckAmount = Functions.removeText(checkoutPage.getPayByCheckAmount()," (tax incl.)");
		Double payByCheckAmountDouble = Functions.removeDollarSignAndReturnDouble(payByCheckAmount);
		assertThat(payByCheckAmountDouble, is(expected_totalTaxInclTotal));
		checkoutPage.selectIAgreeTermsAndConditionsCheckbox();
		assertTrue(checkoutPage.isIAgreeTermsAndConditionsCheckboxSelected());
	}

	String expected_OrderConfirmationMessage = "Your order is confirmed";
	String expected_PaymentMethod = "Payments by check";

	@Test
	public void testCheckOut() {
		testProceedToCheckout();

		OrderPage orderPage = checkoutPage.clickOnOrderWithAnObligationToPayButton();
		
		assertTrue(orderPage.getOrderConfirmationMessage().endsWith(expected_OrderConfirmationMessage.toUpperCase()));
		assertThat(orderPage.getEmail(), is(userEmail));
		assertThat(Functions.removeDollarSignAndReturnDouble(orderPage.getSubtotal()), is(expected_subTotal));
		assertThat(Functions.removeDollarSignAndReturnDouble(orderPage.getTotalTaxInclTotal()), is(expected_totalTaxInclTotal));
		assertThat(orderPage.getPaymentMethod(), is(expected_PaymentMethod));
	}
}
