# language: en
#trying out cleaner scenarios
Feature: Order a product
  As a logged-in user
  I want to be able to choose a product and add it in the shopping cart
  So that I can proceed to checkout and order it

  Background: 
    Given I am in the Home Page
    And I am logged-in with the following credentials "teste@teste.com", "teste", "Teste Testador"

  @InitialValidation
  Scenario: Shows a list of eight products on Home Page
    When I am not logged in
    Then I see a list of 8 available products
    And the cart is empty

  @MainFlow
  Scenario Outline: Add products in the shopping cart
    When I select a product in a given <productPosition>
    And add it into the cart by choosing the size <productSize>, color <productColor> and quantity <productQuantity>
    Then new_the selected product is displayed on review shopping cart screen with the following values: name <productName>, price <productPrice>, size <productSize>, color <productColor>, quantity <productQuantity>, subtotal based on price and quantity

    Examples: 
      | productPosition | productName                   | productPrice | productSize | productColor | productQuantity | userFullName     | productTaxes | shippingValue |
      |               0 | "Hummingbird Printed T-Shirt" | "$19.12"     | "M"         | "Black"      |               2 | "Teste Testador" |         0.00 |          7.00 |

  @MainFlow
  Scenario Outline: Review items added to the shopping cart
    When I select a product in a given <productPosition>
    And add it into the cart by choosing the size <productSize>, color <productColor> and quantity <productQuantity>
    And proceed to checkout
    Then I see all items I have added into the shopping cart as well as their following details: <productName>, <productPrice>, <productSize>, <productColor>, <productQuantity>, subtotal, <shippingValue> and total with and without <productTaxes> included

    Examples: 
      | productPosition | productName                   | productPrice | productSize | productColor | productQuantity | userFullName     | productTaxes | shippingValue |
      |               0 | "Hummingbird Printed T-Shirt" | "$19.12"     | "M"         | "Black"      |               2 | "Teste Testador" |         0.00 |          7.00 |

  @MainFlow
  Scenario Outline: Order the products that are in the shopping cart
    When I have added a product into the shopping cart: position <productPosition>, size <productSize>, color <productColor> and quantity <productQuantity>
    And I click on Proceed to Checkout button on Cart Review Page
    Then I see Total including taxes details based on subtotal <productPrice> * <productQuantity>, <shippingValue> and <productTaxes>
    And I see the logged-in <userFullName> on the registered address on Address Step
    And I am able to go to Shipping Method step by clicking on Continue button to check the <shippingValue>
    And I am able to see and select My Carrier shipping method
    And I am able to go to Payment step by clicking on Continue button
    And I am able to select Pay by Check option and verify pay by check amount based on subtotal <productPrice> * <productQuantity>, <shippingValue> and <productTaxes>
    And I am able to select I agree Terms and Conditions option
    And I am able to complete the product ordering by clicking on Order with and obligation to Pay button

    Examples: 
      | productPosition | productName                   | productPrice | productSize | productColor | productQuantity | userFullName     | productTaxes | shippingValue |
      |               0 | "Hummingbird Printed T-Shirt" | "$19.12"     | "M"         | "Black"      |               2 | "Teste Testador" |         0.00 |          7.00 |

  @MainFlow
  Scenario Outline: Confirm order details are correct
    When I ordered a product that was in the shopping cart: position <productPosition>, size <productSize>, color <productColor> and quantity <productQuantity>
    Then I see the logged-in <userEmail>
    And the subtotal is based on <productPrice> * <productQuantity>
    And the Total including taxes is based on subtotal <productPrice> * <productQuantity>, <shippingValue> and <productTaxes>
    And the <paymentMethod> is the one I selected on Checkout > Payment step

    #TODO: add steps to verify if the order items are correct.
    #TODO: research about cucumber best practices to reuse steps and avoid scenarios with many steps
    Examples: 
      | productPosition | productName                   | productPrice | productSize | productColor | productQuantity | userFullName     | productTaxes | shippingValue | userEmail         | paymentMethod       |
      |               0 | "Hummingbird Printed T-Shirt" | "$19.12"     | "M"         | "Black"      |               2 | "Teste Testador" |         0.00 |          7.00 | "teste@teste.com" | "Payments by check" |
