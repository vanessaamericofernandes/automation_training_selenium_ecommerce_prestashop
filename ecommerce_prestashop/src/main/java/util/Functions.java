package util;

public class Functions {

	public static Double removeDollarSignAndReturnDouble(String text) {
		text = text.replace("$", "");
		return Double.parseDouble(text);
	}

	public static int removeTextItemsAndReturnInt(String text) {
		text = text.replace(" items", "");
		return Integer.parseInt(text);
	}
	
	public static String removeText(String text, String textToRemove) {
		text = text.replace(textToRemove, "");
		return text;
	}
}
