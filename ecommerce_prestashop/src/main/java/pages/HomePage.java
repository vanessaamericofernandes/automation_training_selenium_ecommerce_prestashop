package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

	private WebDriver driver;

	List<WebElement> productsList = new ArrayList();

	private By products = By.className("product-description");
	
	private By productName = By.cssSelector(".product-description a");
	
	private By productPrice = By.className("price");

	private By textProductsInTheChart = By.className("cart-products-count");
	
	private By signInButton = By.cssSelector("#_desktop_user_info span.hidden-sm-down");

	private By loggedInUserFullName = By.cssSelector("#_desktop_user_info span.hidden-sm-down");
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
	}

	public int countProducts() {
		loadProductsList();
		return productsList.size();
	}

	private void loadProductsList() {
		productsList = driver.findElements(products);
	}

	public int getNumberOfProductsInTheCart() {
		String numberOfProductsInTheCart = driver.findElement(textProductsInTheChart).getText();
		numberOfProductsInTheCart = numberOfProductsInTheCart.replace("(", "");
		numberOfProductsInTheCart = numberOfProductsInTheCart.replace(")", "");

		int quantity = Integer.parseInt(numberOfProductsInTheCart);

		return quantity;
	}

	public String getProductName(int index) {
		return driver.findElements(productName).get(index).getText();
	}
	
	public String getProductPrice(int index) {
		return driver.findElements(productPrice).get(index).getText();
	}
	
	public ProductPage selectProduct(int index) {
		driver.findElements(productName).get(index).click();
		return new ProductPage(driver);
	}
	
	public LoginPage clickOnSignInButton() {
		driver.findElement(signInButton).click();
		return new LoginPage(driver);
	}
	
	public boolean isLoggedInUserFullNameDisplayed(String userFullName) {
		return driver.findElement(loggedInUserFullName).getText().contentEquals(userFullName);
	}
	
	public void loadHomePage() {
		driver.get("https://marcelodebittencourt.com/demoprestashop/");		
	}

	public String getPageTitle() {
		return driver.getTitle();
	}
	
	public boolean isTheUserLoggedIn() {
		return !"Sign in".contentEquals(driver.findElement(loggedInUserFullName).getText());
	}
}