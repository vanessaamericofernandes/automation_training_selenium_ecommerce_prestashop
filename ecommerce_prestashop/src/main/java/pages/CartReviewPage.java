package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartReviewPage {

	private WebDriver driver;

	private By productName = By.cssSelector("div.product-line-info a");

	private By productPrice = By.cssSelector("span.price");

	private By productSize = By.xpath("//div[contains(@class,'product-line-grid-body')]//div[3]/span[contains(@class,'value')]");

	private By productColor = By.xpath("//div[contains(@class,'product-line-grid-body')]//div[4]/span[contains(@class,'value')]");

	private By productQuantity = By.cssSelector("input.js-cart-line-product-quantity");

	private By productSubtotal = By.cssSelector("span.product-price strong");

	private By totalNumberOfItems = By.cssSelector("span.js-subtotal");

	private By subtotalTotal = By.cssSelector("#cart-subtotal-products span.value");

	private By shippingValue = By.cssSelector("#cart-subtotal-shipping span.value");

	private By totalTaxExclTotal = By.cssSelector("div.cart-summary-totals div.cart-summary-line:nth-child(1) span.value");

	private By totalTaxInclTotal = By.cssSelector("div.cart-summary-totals div.cart-summary-line:nth-child(2) span.value");

	private By taxesTotal = By.cssSelector("div.cart-summary-totals div.cart-summary-line:nth-child(3) span.value");
	
	private By proceedToCheckoutButton = By.cssSelector("a.btn-primary");

	public CartReviewPage(WebDriver driver) {
		this.driver = driver;
	}

	public String getProductName() {
		return driver.findElement(productName).getText();
	}
	
	public String getProductPrice() {
		return driver.findElement(productPrice).getText();
	}

	public String getProductSize() {
		return driver.findElement(productSize).getText();
	}
	
	public String getProductColor() {
		return driver.findElement(productColor).getText();
	}

	public String getProductQuantity() {
		return driver.findElement(productQuantity).getAttribute("value");
	}
			                  
	public String getProductSubtotal() {
		return driver.findElement(productSubtotal).getText();
	}

	public String getTotalNumberOfItems() {
		return driver.findElement(totalNumberOfItems).getText();
	}
			                  
	public String getSubtotalTotal() {
		return driver.findElement(subtotalTotal).getText();
	}

	public String getShippingValue() {
		return driver.findElement(shippingValue).getText();
	}
	
	public String getTotalTaxExclTotal() {
		return driver.findElement(totalTaxExclTotal).getText();
	}
	
	public String getTotalTaxInclTotal() {
		return driver.findElement(totalTaxInclTotal).getText();
	}
	
	public String getTaxesTotal() {
		return driver.findElement(taxesTotal).getText();
	}
	
	public CheckoutPage clickOnProceedToCheckoutButton() {
		driver.findElement(proceedToCheckoutButton).click();
		return new CheckoutPage(driver);
	}
	
	
}
