package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import util.Functions;

public class OrderPage {
	private WebDriver driver;
	
	private By orderConfirmationMessage = By.cssSelector("#content-hook_order_confirmation h3");
	
	private By email = By.cssSelector("#content-hook_order_confirmation p");
	
	private By subtotal = By.cssSelector("div.order-confirmation-table .order-line .row div.bold");
	
	private By totalTaxInclTotal = By.cssSelector(".order-confirmation-table table tr.total-value td:nth-child(2)");
	
	private By paymentMethod = By.cssSelector("#order-details ul li:nth-child(2)");
	
	public OrderPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getOrderConfirmationMessage() {
		return driver.findElement(orderConfirmationMessage).getText();
	}
	
	public String getEmail() {
		String text = driver.findElement(email).getText();
		text = Functions.removeText(text, "An email has been sent to the ");
		text = Functions.removeText(text, " address.");
		return text;
	}
	
	public String getSubtotal() {
		return driver.findElement(subtotal).getText();
	}
	
	public String getTotalTaxInclTotal() {
		return driver.findElement(totalTaxInclTotal).getText();
	}
	
	public String getPaymentMethod() {
		return Functions.removeText(driver.findElement(paymentMethod).getText(), "Payment method: ");
	}
}
