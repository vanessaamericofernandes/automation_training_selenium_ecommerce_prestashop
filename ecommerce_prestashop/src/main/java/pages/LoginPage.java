package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	
	private WebDriver driver;
	
	private By email = By.name("email");
	
	private By password = By.name("password");
	
	private By signInButton = By.id("submit-login");
	
	private By signOutButton = By.cssSelector("a.logout");
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void fillUserEmail(String userEmail) {
		driver.findElement(email).sendKeys(userEmail);
	}
	
	public void fillUserPassword(String userPassword) {
		driver.findElement(password).sendKeys(userPassword);
	}
	
	public void clickOnSignInButton() {
		driver.findElement(signInButton).click();
	}
	
	public void clickOnSignOutButton() {
		driver.findElement(signOutButton).click();
	}
}
