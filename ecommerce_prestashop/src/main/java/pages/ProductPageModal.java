package pages;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.NoSuchElementException;

public class ProductPageModal {

	private WebDriver driver;

	private By productAddedMessage = By.id("myModalLabel");

	private By productDescription = By.className("product-name");

	private By productPrice = By.cssSelector("div.modal-body .product-price");

	private By productDetails = By.cssSelector("div.divide-right .col-md-6:nth-child(2) span strong");

	private By subTotal = By.cssSelector(".cart-content p:nth-child(2) span.value");

	private By proceedToCheckoutButton = By.cssSelector(".cart-content-btn a");

	public ProductPageModal(WebDriver driver) {
		this.driver = driver;
	}

	public String getProductAddedMessage() {

		FluentWait wait = new FluentWait(driver).withTimeout(Duration.ofSeconds(5)).pollingEvery(Duration.ofSeconds(1))
				.ignoring(NoSuchElementException.class);

		wait.until(ExpectedConditions.visibilityOfElementLocated(productAddedMessage));

		return driver.findElement(productAddedMessage).getText();
	}

	public String getProductDescription() {
		return driver.findElement(productDescription).getText();
	}

	public String getProductPrice() {
		return driver.findElement(productPrice).getText();
	}

	public String getSize() {
		return driver.findElements(productDetails).get(0).getText();
	}

	public String getColor() {
		if (driver.findElements(productDetails).size() == 3)
			return driver.findElements(productDetails).get(1).getText();
		else
			return "N/A";
	}

	public int getQuantity() {
		if (driver.findElements(productDetails).size() == 3)
			return Integer.parseInt(driver.findElements(productDetails).get(2).getText());
		else
			return Integer.parseInt(driver.findElements(productDetails).get(1).getText());
	}

	public String getSubTotal() {
		return driver.findElement(subTotal).getText();
	}

	public CartReviewPage clickOnProceedToCheckoutButton() {
		driver.findElement(proceedToCheckoutButton).click();
		return new CartReviewPage(driver);
	}

}
