package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ProductPage {
	private WebDriver driver;
	
	private By productName = By.className("h1");
	
	private By productPrice = By.xpath("//*[@id=\"main\"]/div[1]/div[2]/div[1]/div[2]/div/span[1]");
	
	private By productSize = By.id("group_1");
	
	private By productColor = By.xpath("//*[@id=\"group_2\"]/li[2]/label/input");
	
	private By dynamicProductColor = By.xpath("//span[text() = 'Black']/../../input[@class='input-color']");
	
	private By quantity = By.id("quantity_wanted");
	
	private By addToCartButton = By.className("add-to-cart");
		
	public ProductPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getProductName() {
		return driver.findElement(productName).getText();
	}
	
	public String getProductPrice() {
		return driver.findElement(productPrice).getText();
	}
	
	public void selectSizeFromDropdown(String option) {
		findSizeDropdown().selectByVisibleText(option);
	}
	
	public List<String> getSizeOptions(){
		List<WebElement> selectedElements = findSizeDropdown().getAllSelectedOptions();
		
		List<String> listOptions = new ArrayList();
		for(WebElement element : selectedElements) {
			listOptions.add(element.getText());
		}
		return listOptions;
	}
	
	public Select findSizeDropdown() {
		return new Select(driver.findElement(productSize));
	}
	
	public void selectBlackColor() {
		driver.findElement(productColor).click();
	}
	
	public void selectColor(String color) {
		driver.findElement(By.xpath("//span[text() = '" + color + "']/../../input[@class='input-color']")).click();  
	}
	
	public void selectQuantity(int productQuantity) {
		driver.findElement(quantity).clear();
		driver.findElement(quantity).sendKeys(Integer.toString(productQuantity));
	}
	
	public ProductPageModal clickOnAddToCartButton() {
		driver.findElement(addToCartButton).click();
		return new ProductPageModal(driver);
	}
}
