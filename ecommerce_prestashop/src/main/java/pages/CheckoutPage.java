package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckoutPage {
	private WebDriver driver;

	private By totalTaxInclTotal = By.cssSelector("div.cart-total span.value");

	private By clientName = By.cssSelector("div.address");
	
	private By addressContinueButton = By.name("confirm-addresses");
	
	private By shippingValue = By.cssSelector("span.carrier-price");
	
	private By shippingContinueButton = By.name("confirmDeliveryOption");
	
	private By payByCheckRadioButton = By.id("payment-option-1");
	
	private By payByCheckAmount = By.cssSelector("#payment-option-1-additional-information > section > dl > dd:nth-child(2)");
	
	private By iAgreeTermsAndConditionsCheckbox = By.id("conditions_to_approve[terms-and-conditions]");
	
	private By orderWithAnObligationToPayButton = By.cssSelector("#payment-confirmation button");
	
	public CheckoutPage(WebDriver driver) {
		this.driver = driver;
	}

	public String getTotalTaxInclTotal() {
		return driver.findElement(totalTaxInclTotal).getText();
	}

	public String getClientName() {
		return driver.findElement(clientName).getText();
	}
	
	public void clickOnAddressContinueButton() {
		driver.findElement(addressContinueButton).click();
	}
	
	public String getShippingValue() {
		return driver.findElement(shippingValue).getText();
	}
	
	public void clickOnContinueToPaymentButton() {
		driver.findElement(shippingContinueButton).click();
	}
	
	public void selectPayByCheckRadioButton() {
		driver.findElement(payByCheckRadioButton).click();
	}
	
	public String getPayByCheckAmount() {
		return driver.findElement(payByCheckAmount).getText();
	}
	
	public void selectIAgreeTermsAndConditionsCheckbox() {
		driver.findElement(iAgreeTermsAndConditionsCheckbox).click();
	}
	
	public boolean isIAgreeTermsAndConditionsCheckboxSelected() {
		return driver.findElement(iAgreeTermsAndConditionsCheckbox).isSelected();
	}
	
	public OrderPage clickOnOrderWithAnObligationToPayButton() {
		driver.findElement(orderWithAnObligationToPayButton).click();
		return new OrderPage(driver);
	}
}
