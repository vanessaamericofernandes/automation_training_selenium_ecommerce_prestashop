# language: en
Feature: Order a product
  As a logged-in user
  I want to be able to choose a product and add it in the shopping cart
  So that I can proceed to checkout and order it

  @InitialValidation
  Scenario: Shows a list of eight products on Home Page
    Given I am in the Home Page
    When I am not logged in
    Then I see a list of 8 available products
    And the cart is empty

  @MainFlow
  Scenario Outline: Add products into the shopping cart
    Given I am in the Home Page
    When I am logged-in <userEmail>, <userPassword>, <userFullName>
    And select the product in position <productPosition>
    And the product name on the main screen is <productName>
    And the product price on the main screen is <productPrice>
    And I add the product in the cart by choosing the size <productSize>, color <productColor> and quantity <productQuantity>
    Then the selected product is displayed on review shopping cart screen with the following values: name <productName>, price <productPrice>, size <productSize>, color <productColor>, quantity <productQuantity>, subtotal based on price and quantity

    Examples: 
      | productPosition | productName                   | productPrice | productSize | productColor | productQuantity | userFullName     | userEmail         | userPassword |
      |               0 | "Hummingbird Printed T-Shirt" | "$19.12"     | "M"         | "Black"      |               2 | "Teste Testador" | "teste@teste.com" | "teste"      |

  @MainFlow
  Scenario Outline: Review items added to the shopping cart
    Given I am in the Home Page
    And I am logged-in <userEmail>, <userPassword>, <userFullName>
    And select the product in position <productPosition>
    And the product name on the main screen is <productName>
    And the product price on the main screen is <productPrice>
    And I add the product in the cart by choosing the size <productSize>, color <productColor> and quantity <productQuantity>
    When I click on Proceed to Checkout button
    Then I see all items I have added into the shopping cart as well as their following details: <productName>, <productPrice>, <productSize>, <productColor>, <productQuantity>, subtotal, <shippingValue> and total with and without <productTaxes> included

    Examples: 
      | productPosition | productName                   | productPrice | productSize | productColor | productQuantity | userFullName     | productTaxes | shippingValue | userEmail         | userPassword |
      |               0 | "Hummingbird Printed T-Shirt" | "$19.12"     | "M"         | "Black"      |               2 | "Teste Testador" |         0.00 |          7.00 | "teste@teste.com" | "teste"      |

  @MainFlow
  Scenario Outline: Order the products that are in the shopping cart
    Given I am in the Home Page
    And I am logged-in <userEmail>, <userPassword>, <userFullName>
    And select the product in position <productPosition>
    And the product name on the main screen is <productName>
    And the product price on the main screen is <productPrice>
    And I add the product in the cart by choosing the size <productSize>, color <productColor> and quantity <productQuantity>
    And I click on Proceed to Checkout button
    And I see all items I have added into the shopping cart as well as their following details: <productName>, <productPrice>, <productSize>, <productColor>, <productQuantity>, subtotal, <shippingValue> and total with and without <productTaxes> included
    When I click on Proceed to Checkout button on Cart Review Page
    Then I see Total including taxes details based on subtotal <productPrice> * <productQuantity>, <shippingValue> and <productTaxes>
    And I see the logged-in <userFullName> on the registered address on Address Step
    And I am able to go to Shipping Method step by clicking on Continue button to check the <shippingValue>
    And I am able to see and select My Carrier shipping method
    And I am able to go to Payment step by clicking on Continue button
    And I am able to select Pay by Check option and verify pay by check amount based on subtotal <productPrice> * <productQuantity>, <shippingValue> and <productTaxes>
    And I am able to select I agree Terms and Conditions option
    And I am able to complete the product ordering by clicking on Order with and obligation to Pay button

    Examples: 
      | productPosition | productName                   | productPrice | productSize | productColor | productQuantity | userFullName     | productTaxes | shippingValue | userEmail         | userPassword |
      |               0 | "Hummingbird Printed T-Shirt" | "$19.12"     | "M"         | "Black"      |               2 | "Teste Testador" |         0.00 |          7.00 | "teste@teste.com" | "teste"      |

  @MainFlow
  Scenario Outline: Confirm order details are correct
    When I am in the Home Page
    And I am logged-in <userEmail>, <userPassword>, <userFullName>
    And select the product in position <productPosition>
    And the product name on the main screen is <productName>
    And the product price on the main screen is <productPrice>
    And I add the product in the cart by choosing the size <productSize>, color <productColor> and quantity <productQuantity>
    And I click on Proceed to Checkout button
    And I see all items I have added into the shopping cart as well as their following details: <productName>, <productPrice>, <productSize>, <productColor>, <productQuantity>, subtotal, <shippingValue> and total with and without <productTaxes> included
    And I click on Proceed to Checkout button on Cart Review Page
    And I see Total including taxes details based on subtotal <productPrice> * <productQuantity>, <shippingValue> and <productTaxes>
    And I see the logged-in <userFullName> on the registered address on Address Step
    And I am able to go to Shipping Method step by clicking on Continue button to check the <shippingValue>
    And I am able to see and select My Carrier shipping method
    And I am able to go to Payment step by clicking on Continue button
    And I am able to select Pay by Check option and verify pay by check amount based on subtotal <productPrice> * <productQuantity>, <shippingValue> and <productTaxes>
    And I am able to select I agree Terms and Conditions option
    And I am able to complete the product ordering by clicking on Order with and obligation to Pay button
    Then I see the logged-in <userEmail>
    And the subtotal is based on <productPrice> * <productQuantity>
    And the Total including taxes is based on subtotal <productPrice> * <productQuantity>, <shippingValue> and <productTaxes>
    And the <paymentMethod> is the one I selected on Checkout > Payment step

    #TODO: add steps to verify if the order items are correct.
    #TODO: research about cucumber best practices to reuse steps and avoid scenarios with many steps
    Examples: 
      | productPosition | productName                   | productPrice | productSize | productColor | productQuantity | userFullName     | productTaxes | shippingValue | userEmail         | userPassword | paymentMethod       |
      |               0 | "Hummingbird Printed T-Shirt" | "$19.12"     | "M"         | "Black"      |               2 | "Teste Testador" |         0.00 |          7.00 | "teste@teste.com" | "teste"      | "Payments by check" |
      |               0 | "Hummingbird Printed T-Shirt" | "$19.12"     | "XL"        | "White"      |               3 | "Teste Testador" |         0.00 |          7.00 | "teste@teste.com" | "teste"      | "Payments by check" |
      |               1 | "Hummingbird Printed Sweater" | "$28.72"     | "XL"        | "N/A"        |               3 | "Teste Testador" |         0.00 |          7.00 | "teste@teste.com" | "teste"      | "Payments by check" |
